/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.auth.controller;
import com.gemframework.common.annotation.ApiLog;
import com.gemframework.common.utils.GemValidateUtils;
import com.gemframework.model.common.ApiToken;
import com.gemframework.model.common.BaseResultData;
import com.gemframework.model.response.AccessTokenResponse;
import com.gemframework.model.response.RefreshTokenResponse;
import com.gemframework.modules.prekit.auth.entity.request.AccountPasswordAuthRequest;
import com.gemframework.modules.prekit.auth.entity.request.RefreshAccessTokenRequest;
import com.gemframework.service.TokenService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

@RestController
public class AuthTokenApi {

	@Resource
	private TokenService tokenService;


	@ApiLog(name = "获取token")
	@GetMapping("getAccessToken")
	public BaseResultData getAccessToken(@RequestBody AccountPasswordAuthRequest request) {
		//参数校验器
		GemValidateUtils.GemValidate(request);
		//获取token
		ApiToken token = tokenService.getAccessToken(request.getMemberId());
		AccessTokenResponse data = AccessTokenResponse.builder()
				.accessToken(token.getAccessToken())
				.expiresIn(token.getAccessTokenExpireTime().getTime()/1000-new Date().getTime()/1000)
				.refreshToken(token.getRefreshToken())
				.object(request.getMemberId())
				.build();
		return BaseResultData.SUCCESS(data);
	}

	@ApiLog(name = "刷新token")
	@GetMapping("refreshAccessToken")
	public BaseResultData refreshAccessToken(@RequestBody RefreshAccessTokenRequest request) {
		//参数校验器
		GemValidateUtils.GemValidate(request);
		ApiToken token = tokenService.refreshToken(request.getMemberId(),request.getRefreshToken());
		RefreshTokenResponse data = RefreshTokenResponse.builder()
				.accessToken(token.getAccessToken())
				.expiresIn(token.getAccessTokenExpireTime().getTime()/1000-new Date().getTime()/1000)
				.refreshToken(token.getRefreshToken())
				.build();
		return BaseResultData.SUCCESS(data);
	}

}
