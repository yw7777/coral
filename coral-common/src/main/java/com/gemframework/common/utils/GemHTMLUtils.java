package com.gemframework.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Title: HTMLUtils
 * @Package: com.gemframework.common.utils
 * @Date: 2020-07-13 23:39:09
 * @Version: v1.0
 * @Description: HTML处理工具
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
public class GemHTMLUtils {

    /**
     * 过滤所有HTML 标签
     * @param htmlStr
     * @return
     */
    public static String filterHTMLTag(String htmlStr) {
        //定义HTML标签的正则表达式
        String reg_html = "<[^>]+>";
        Pattern pattern = Pattern.compile(reg_html, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(htmlStr);
        htmlStr = matcher.replaceAll(""); //过滤html标签
        return htmlStr;
    }

    /**
     * 过滤标签，通过标签名
     * @param htmlStr
     * @param tagName
     * @return
     */
    public static String filterTagByName(String htmlStr, String tagName) {
        String reg_html = "<" + tagName + "[^>]*?>[\\s\\S]*?<\\/" + tagName + ">";
        Pattern pattern = Pattern.compile(reg_html, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(htmlStr);
        htmlStr = matcher.replaceAll(""); //过滤html标签
        return htmlStr;
    }

    /**
     * 过滤标签上的 style 样式
     * @param htmlStr
     * @return
     */
    public static String filterHTMLTagInStyle(String htmlStr) {
        String reg_html = "style=('|\")(.*?)('|\")";
        Pattern pattern = Pattern.compile(reg_html, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(htmlStr);
        htmlStr = matcher.replaceAll(""); //过滤html标签
        return htmlStr;
    }

    /**
     * 替换表情
     * @param htmlStr
     * @return
     */
    public static String filterHTMLFace(String htmlStr) {
        String reg_html = "\\[em_\\d{1,}\\]";
        Pattern pattern = Pattern.compile(reg_html, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(htmlStr);
        if (matcher.find()) {
            matcher.reset();
            while (matcher.find()) {
                String num = matcher.group(0);
                String number = num.substring(num.lastIndexOf('_') + 1, num.length() - 1);
                htmlStr = htmlStr.replace(num, "<img src='/face/arclist/" + number + ".gif' border='0' />");
            }
        }
        return htmlStr;
    }

    /**
     * 过滤危险字符串字符防止XSS
     * @param value
     * @return
     */
    public static String filterDangerString(String value) {
        if (value == null) {
            return null;
        }
        value = value.replaceAll("\\{", "{");
        value = value.replaceAll("<", "&lt;");
        value = value.replaceAll(">", "&gt;");
        value = value.replaceAll("&", "&amp;");
        value = value.replaceAll("\t", "    ");
        value = value.replaceAll("\r\n", "\n");
        value = value.replaceAll("\n", "<br/>");
        value = value.replaceAll("'", "&#39;");
        value = value.replaceAll("\\\\", "&#92;");
        value = value.replaceAll("\"", "&quot;");
        value = value.replaceAll("\\}", "}").trim();
        return value;
    }
    /**
     * 过滤转义字符还原成html字符
     * 将被XSS转义的危险字符串还原
     * @param value
     * @return
     */
    public static String filterXssString(String value) {
        if (value == null) {
            return null;
        }
        value = value.replaceAll("&amp;", "&");
        value = value.replaceAll("&lt;", "<");
        value = value.replaceAll("&gt;", ">");
        value = value.replaceAll("&nbsp;", " ");
        value = value.replaceAll("&#39;", "\'");
        value = value.replaceAll("&quot;", "\"").trim();
        return value;
    }


    public static void main(String[] args) {
        String html = "<script>alert('test');</script><img src='/face/arclist/5.gif' border='0' /><div style='position:fixs;s'></div><style>body{color:#fff;}</style><Style>body{color:#fff;}</Style><STYLE>body{color:#fff;}</STYLE>";
        System.out.println("html=" + html);
        html = GemHTMLUtils.filterTagByName(html, "style");
        System.out.println("html=" + html);
        html = GemHTMLUtils.filterTagByName(html, "script");
        System.out.println("html=" + html);
        html = GemHTMLUtils.filterHTMLTagInStyle(html);
        System.out.println("html=" + html);

//        jdbc:mysql://localhost:3306/coral-tenant?useUnicode=true&amp;characterEncoding=utf-8&amp;useSSL=false&amp;serverTimezone=GMT%2b8
    }

}