package com.gemframework.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gemframework.model.entity.po.Datasource;
import org.springframework.stereotype.Repository;


/**
 * @Title: DatasourceMapper
 * @Package: com.gemframework.mapper
 * @Date: 2020-06-29 22:10:14
 * @Version: v1.0
 * @Description: DatasourceMapper
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Repository
public interface DatasourceMapper extends BaseMapper<Datasource> {
}